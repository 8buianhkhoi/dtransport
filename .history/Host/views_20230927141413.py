from django.shortcuts import render
from django.views import View
from django.contrib.auth.hashers import make_password
# Create your views here.

def base_page(request):
    return render(request, 'Host/base_page.html')

class Company(View):
    def get(self, request):
        return render(request, 'Host/company.html')
    
    def post(self, request):
        user_name_company = request.POST('input_user_name_new_company', None)
        password_company = request.POST('input_pass_new_company', None)
        name_company = request.POST('input_name_new_company', None)
        country_company = 'VN'
        province_company = request.POST('select_province_new_company', None)
        district_company = request.POST('select_district_new_company', None)
        ward_company = request.POST('select_ward_new_company', None)
        tin_company = request.POST('input_tin_new_company', None)
        gmail_company = request.POST('input_gmail_new_company', None)
        tel_company = request.POST('input_tel_new_company', None)
        account_bank_company = request.POST('input_account_bank_new_company', None)
        name_bank_company = request.POST('input_name_bank_new_company', None)
        name_boss_company = request.POST('input_name_boss_new_company', None)

        return render(request, 'Host/company.html')
