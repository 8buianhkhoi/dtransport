from django.urls import path

from . import views 

app_name = 'Host_app'

urlpatterns = [
    path('', views.base_page, name = 'Host_base_page'),
    path('company/', views.company, name = 'Host_company')
]