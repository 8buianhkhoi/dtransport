from django.shortcuts import render
from django.views import View
# Create your views here.

def base_page(request):
    return render(request, 'Host/base_page.html')

class Company(View):
    def get(self, request):
        return render(request, 'Host/company.html')
