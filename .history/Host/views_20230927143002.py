from django.shortcuts import render
from django.views import View
from django.contrib.auth.hashers import make_password

from . import exec_data
# Create your views here.

def base_page(request):
    return render(request, 'Host/base_page.html')

class Company(View):
    def get(self, request):
        return render(request, 'Host/company.html')
    
    def post(self, request):
        user_name_company = request.POST('input_user_name_new_company', None)
        password_company = request.POST('input_pass_new_company', None)
        name_company = request.POST('input_name_new_company', None)
        country_company = 'VN'
        province_company = request.POST('select_province_new_company', None)
        district_company = request.POST('select_district_new_company', None)
        ward_company = request.POST('select_ward_new_company', None)
        tin_company = request.POST('input_tin_new_company', None)
        gmail_company = request.POST('input_gmail_new_company', None)
        tel_company = request.POST('input_tel_new_company', None)
        account_bank_company = request.POST('input_account_bank_new_company', None)
        name_bank_company = request.POST('input_name_bank_new_company', None)
        name_boss_company = request.POST('input_name_boss_new_company', None)
        name_contact_person_company = request.POST('input_name_contact_person_new_company', None)
        tel_contact_person_company = request.POST('input_tel_contact_person_new_company', None)
        gmail_contact_person_company = request.POST('input_gmail_contact_person_new_company', None)
        url_company = request.POST('input_url_contact_person_new_company', None)

        para_dict = {'user_name_para' : user_name_company, 'password_para' : make_password(password_company), 'name_para' : name_company, 'country_para' : country_company, 'province_para' : province_company, 'district_para' : district_company, 'ward_para' : ward_company, 'tin_para' : tin_company, 'gmail_para' : gmail_company, 'tel_para' : tel_company, 'account_bank_para' : account_bank_company, 'name_bank_para' : name_bank_company, 'name_boss_para' : name_boss_company, 'name_contact_person_para' : name_contact_person_company, 'tel_contact_person_para' : tel_contact_person_company, 'gmail_contact_person_para' : gmail_contact_person_company, 'url_company_para' : url_company}

        return render(request, 'Host/company.html')
