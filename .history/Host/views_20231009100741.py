from dotenv import load_dotenv
import os
import jwt 
import datetime 

from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.hashers import make_password

from . import exec_data
# Create your views here.
# This function try to check token. If token already is use. It continue browsing or redirect to homepage
def check_login_required(token_str):
    load_dotenv()
    secret_key = os.getenv('secret_key')
    decode_token = jwt.decode(token_str, secret_key, algorithms = ["HS256"])
    current_time = datetime.datetime.now()
    end_time_token = datetime.datetime.strptime(decode_token['end_time'], f'%d-%m-%Y %H:%M:%S')
    if current_time < end_time_token:
        role_user_temp = decode_token['role_user']
        if role_user_temp == 'host':
            return True
        else:
            return 'NP'
    else:
        return 'ET'

def base_page(request):
    # Check token
    full_token_str = request.session.get('token_dt2023_web_demo', None)
    if full_token_str is None or full_token_str == '':
        return redirect('Homepage_app:Homepage_homepage')
    check_token_str = check_login_required(full_token_str)
    if check_token_str != True:
        return redirect('Homepage_app:Homepage_homepage')
    # End check token string

    return render(request, 'Host/base_page.html')

class Company(View):
    def get(self, request):
        # Check token
        full_token_str = request.session.get('token_dt2023_web_demo', None)
        if full_token_str is None or full_token_str == '':
            return redirect('Homepage_app:Homepage_homepage')
        check_token_str = check_login_required(full_token_str)
        if check_token_str != True:
            return redirect('Homepage_app:Homepage_homepage')
        # End check token string

        return render(request, 'Host/company.html')
    
    def post(self, request):
        return_para = {}

        user_name_company = request.POST.get('input_user_name_new_company', None)
        password_company = request.POST.get('input_pass_new_company', None)
        name_company = request.POST.get('input_name_new_company', None)
        country_company = 'VN'
        province_company = request.POST.get('select_province_new_company', None)
        district_company = request.POST.get('select_district_new_company', None)
        ward_company = request.POST.get('select_ward_new_company', None)
        tin_company = request.POST.get('input_tin_new_company', None)
        gmail_company = request.POST.get('input_gmail_new_company', None)
        tel_company = request.POST.get('input_tel_new_company', None)
        account_bank_company = request.POST.get('input_account_bank_new_company', None)
        name_bank_company = request.POST.get('input_name_bank_new_company', None)
        name_boss_company = request.POST.get('input_name_boss_new_company', None)
        name_contact_person_company = request.POST.get('input_name_contact_person_new_company', None)
        tel_contact_person_company = request.POST.get('input_tel_contact_person_new_company', None)
        gmail_contact_person_company = request.POST.get('input_gmail_contact_person_new_company', None)
        url_company = request.POST.get('input_url_contact_person_new_company', None)

        para_dict = {'user_name_para' : user_name_company, 'password_para' : make_password(password_company), 'name_para' : name_company, 'country_para' : country_company, 'province_para' : province_company, 'district_para' : district_company, 'ward_para' : ward_company, 'tin_para' : tin_company, 'gmail_para' : gmail_company, 'tel_para' : tel_company, 'account_bank_para' : account_bank_company, 'name_bank_para' : name_bank_company, 'name_boss_para' : name_boss_company, 'name_contact_person_para' : name_contact_person_company, 'tel_contact_person_para' : tel_contact_person_company, 'gmail_contact_person_para' : gmail_contact_person_company, 'url_company_para' : url_company}

        ins_new_company = exec_data.ins_new_company(**para_dict)

        if ins_new_company == True:
            return_para['notification_ins_new_company'] = 'Thêm công ty thành công'
        else:
            return_para['notification_ins_new_company'] = 'Thêm công ty thất bại'

        return render(request, 'Host/company.html', return_para)
