from django.shortcuts import render
from django.views import View
from django.contrib.auth.hashers import make_password
# Create your views here.

def base_page(request):
    return render(request, 'Host/base_page.html')

class Company(View):
    def get(self, request):
        return render(request, 'Host/company.html')
    
    def post(self, request):

        return render(request, 'Host/company.html')
