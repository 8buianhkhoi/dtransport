from Homepage.models import *

def ins_new_company(user_name_para, password_para, name_para, country_para, province_para, district_para, ward_para, tin_para, gmail_para, tel_para, account_bank_para, name_bank_para, name_boss_para, name_contact_person_para, tel_contact_person_para, gmail_contact_person_para, url_company_para):
    try:
        ins_new_record = Company_tb(username = user_name_para, password = password_para, full_name = name_para, country = country_para, province = province_para, district = district_para, ward = ward_para, tin = tin_para, gmail = gmail_para, tel = tel_para, account_bank = account_bank_para, name_bank = name_bank_para, name_boss = name_boss_para, name_contact_person = name_contact_person_para, tel_contact_person = tel_contact_person_para, gmail_contact_person = gmail_contact_person_para, url_website = url_company_para)
        ins_new_record.save()

        return True
    except Exception as e:
        print(e)
        return False