function load_district(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    let province = document.getElementById('choose__province__new__customer').value
    $.ajax({
        url:'/sales/get-all-district/',
        method: 'POST',
        data: {province:province, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementById('choose__district__new__customer')
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function load_ward(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 

    let district = document.getElementById('choose__district__new__customer').value
    let province = document.getElementById('choose__province__new__customer').value
    $.ajax({
        url:'/sales/get-all-ward/',
        method: 'POST',
        data: {province:province, district :district, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementById('choose__ward__new__customer')
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function on_load_province(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    $.ajax({
        url:'/sales/get-all-province/',
        method: 'POST',
        data: {
            csrfmiddlewaretoken : csrf_token,
        },
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('new__province__customer__select');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}