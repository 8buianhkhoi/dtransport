from django.shortcuts import render
from django.views import View 

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Sales/homepage.html')

class Customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer.html')

class Add_new_customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer_add_new.html')