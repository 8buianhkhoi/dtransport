from django.urls import path

from . import views 

app_name = 'Sales_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Sales_homepage' ),
    path('customer/', views.Customer.as_view(), name='Sales_customer'),
    path('customer/add-new/', views.Add_new_customer.as_view(), name='Sales_add_new_customer')
]