from django.shortcuts import render, redirect
from django.views import View 
from django.http import JsonResponse

from . import exec_data

# Create your views here.

# 3 function help to load and get division
def get_all_province(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def get_district_by_province(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def get_ward_by_district_province(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

def log_out(request):
    request.session.clear()
    return redirect('Homepage_app:Homepage_homepage')

class Homepage(View):
    def get(self, request):
        return render(request, 'Sales/homepage.html')

class Customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer.html')

class Add_new_customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer_add_new.html')
    def post(self, request):
        new_gmail = request.POST.get('new_customer_gmail', None)
        new_tel = request.POST.get('new_customer_tel', None)
        new_province = request.POST.get('new_customer_province', None)
        new_district = request.POST.get('new_customer_district', None)
        new_ward = request.POST.get('new_customer_ward', None)
        new_full_name = request.POST.get('new_customer_full_name', None)
        new_gender = request.POST.get('new_customer_gender', None)
        new_type = request.POST.get('new_customer_type', None)
        new_status = request.POST.get('new_customer_status', None)

        return_para = {'new_gmail_para' : new_gmail, 'new_tel_para' : new_tel, 'new_province_para' : new_province, 'new_district_para' : new_district, 'new_ward_para' : new_ward, 'new_full_name_para' : new_full_name, 'new_gender_para' : new_gender, 'new_type_para' : new_type, 'new_status_para' : new_status}
        ins_new_customer = exec_data.add_new_customer(**return_para)

        context_return = {}
        if ins_new_customer == True:
            context_return['ins_new_customer'] = 'Thêm khách hàng mới thành công'
        else:
            context_return['ins_new_customer'] = 'Thêm khách hàng mới thất bại'

        return render(request, 'Sales/tab_customer_add_new.html', context_return)
    
class Show_all_customer(View):
    def get(self, request):
        context_return = {}
        get_all_customer = exec_data.get_all_customer_by_id_sale()

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer

        return render(request, 'Sales/tab_customer_show_all.html', context_return)
    
class Update_status_customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer_update_status.html')
    def post(self, request):
        context_return = {}

        get_id_customer = request.POST.get('search_id_customer', None)
        get_status_update = request.POST.get('select_status_customer', None)

        update_status = exec_data.update_status_customer(get_id_customer, get_status_update)

        if update_status == True:
            context_return['update_status_message'] = 'Cập nhật thành công'
        else:
            context_return['update_status_message'] = 'Cập nhật thất bại'
        
        return render(request, 'Sales/tab_customer_update_status.html', context_return)

class Contract(View):
    def get(self, request):
        id_sales = request.session.get('id_role_user_dt2023', None)
        all_customer = exec_data.get_all_customer_by_id_sale(id_sales)

        return render(request, 'Sales/tab_contract.html', {'all_customer' : all_customer})

    def post(self, request):
        id_sales = request.session.get('id_role_user_dt2023', None)
        all_customer = exec_data.get_all_customer_by_id_sale(id_sales)
        
        new_start_time = request.POST.get('input_start_time_contract', None)
        new_end_time =  request.POST.get('input_end_time_contract', None)
        new_date_sign = request.POST.get('input_date_sign_contract', None)
        new_cost = request.POST.get('input_cost_contract', None)
        new_url_scan = request.POST.get('input_url_contract', None)
        new_note = request.POST.get('input_note_contract', None)
        new_id_customer = request.POST.get('input_id_customer', None)

        return_para = {'id_sale_para' : id_sales, 'id_customer_para' : new_id_customer, 'start_time_para' : new_start_time, 'end_time_para' : new_end_time, 'date_sign_para' : new_date_sign, 'cost_para' : new_cost, 'link_drive_scan_para' : new_url_scan, 'note_para' : new_note}
        ins_new_contract = exec_data.ins_new_contract(**return_para)

class Account(View):
    def get(self,request):
        return render(request, 'Sales/tab_account.html')
