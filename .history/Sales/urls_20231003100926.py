from django.urls import path

from . import views 

app_name = 'Sales_app'

urlpatterns = [
    path('get-all-province/', views.get_all_province, name = 'Sales_get_all_province'),
    path('get-all-district/', views.get_district_by_province, name = 'Sales_get_all_district'),
    path('get-all-ward/', views.get_ward_by_district_province, name = 'Sales_get_all_ward'),
    path('', views.Homepage.as_view(), name = 'Sales_homepage' ),
    path('customer/', views.Customer.as_view(), name='Sales_customer'),
    path('customer/add-new/', views.Add_new_customer.as_view(), name='Sales_add_new_customer'),
    path('customer/show-all/', views.Show_all_customer.as_view(), name = 'Sales_show_all_customer'),
    path('customer/update-status-customer/', views.Update_status_customer.as_view(), name = 'Sales_update_status_customer'),
    path('contract/', views.Contract.as_view(), name='Sales_contract'),
    path('account/', views.Account.as_view(), name = 'Sales_account')
]