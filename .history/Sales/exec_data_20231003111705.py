from Homepage.models import *

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def add_new_customer(new_gmail_para, new_tel_para, new_province_para, new_district_para, new_ward_para, new_full_name_para, new_gender_para, new_type_para, new_status_para):
    try:
        # TODO : code here, id_customer ở cuối cần gán bằng id company + sdt khách hàng
        ins_new_customer = Customer_tb(gmail = new_gmail_para, tel = new_tel_para, country = 'VN', province = new_province_para, district = new_district_para, ward = new_ward_para, full_name = new_full_name_para, gender = new_gender_para, types = new_type_para, id_customer = '0', status = new_status_para)
        ins_new_customer.save()

        return True
    except:
        return False
    
def get_all_customer_by_id_sale(id_sale_para):
    try:
        all_customer = Customer_tb.objects.filter(id_sale = id_sale_para).values()
        return all_customer
    except:
        return False
    
def update_status_customer(id_customer_para, status_update_para):
    try:
        update_status = Customer_tb.objects.filter(id_customer = id_customer_para).update(status = status_update_para)
        return True
    except:
        return False

def ins_new_contract(id_sale_para, id_customer_para, start_time_para, end_time_para, date_sign_para, cost_para, link_drive_scan_para, note_para):
    try:
        id_company = Sale_tb.objects.filter(id = id_sale_para).values()[0]['id_company_id']
        instance_id_company = Company_tb.objects.get(pk = id_company)
        instance_id_customer = Customer_tb.objects.get(pk = id_customer_para)
        length_all_contract = Contract_tb.objects.count()
        
        if length_all_contract >= 99999999:
            return False 
        
        no_contract_random = 'Co' + ((9 - len(str(length_all_contract)))*'0') + str(length_all_contract)

        new_contract = Contract_tb(id_company = instance_id_company, id_customer = instance_id_customer, start_time = start_time_para, end_time = end_time_para, date_sign = date_sign_para, cost = cost_para, link_drive_scan = link_drive_scan_para, no_contract = no_contract_random, note = note_para)
    except:
        return False