from django.urls import path

from . import views 

app_name = 'Sales_app'

urlpatterns = [
    path('', views.Homepage, name = 'Sales_homepage' )
]