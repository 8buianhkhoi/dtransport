from django.shortcuts import render
from django.views import View 
from django.http import JsonResponse

from . import exec_data

# Create your views here.

# 3 function help to load and get division
def get_all_province(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def get_district_by_province(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def get_ward_by_district_province(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

class Homepage(View):
    def get(self, request):
        return render(request, 'Sales/homepage.html')

class Customer(View):
    def get(self, request):
        return render(request, 'Sales/tab_customer.html')

class Add_new_customer(View):
    def get(self, request):
        print('--------------')
        print(dir(request.session))
        return render(request, 'Sales/tab_customer_add_new.html')
    def post(self, request):
        new_gmail = request.POST.get('new_customer_gmail', None)
        new_tel = request.POST.get('new_customer_tel', None)
        new_province = request.POST.get('new_customer_province', None)
        new_district = request.POST.get('new_customer_district', None)
        new_ward = request.POST.get('new_customer_ward', None)
        new_full_name = request.POST.get('new_customer_full_name', None)
        new_gender = request.POST.get('new_customer_gender', None)
        new_type = request.POST.get('new_customer_type', None)

        return_para = {'new_gmail_para' : new_gmail, 'new_tel_para' : new_tel, 'new_province_para' : new_province, 'new_district_para' : new_district, 'new_ward_para' : new_ward, 'new_full_name_para' : new_full_name, 'new_gender_para' : new_gender, 'new_type_para' : new_type}
        ins_new_customer = exec_data.add_new_customer(return_para)