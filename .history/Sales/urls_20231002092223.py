from django.urls import path

from . import views 

app_name = 'Sales_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Sales_homepage' ),
    path('customer/', views.Customer.as_view(), name='Sales_customer')
]