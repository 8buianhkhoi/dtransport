from Homepage.models import *

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def add_new_customer()