from Homepage.models import *

def get_all_order_need_delivery(id_driver_para):
    try:
        all_order_need_delivery = Order_tb.objects.filter(status = 'Need-Delivery', id_driver = id_driver_para).values()
        return all_order_need_delivery
    except:
        return False

def get_all_order_in_delivery(id_driver_para):
    try:
        all_order_in_delivery = Order_tb.objects.filter(status = 'Delivery', id_driver = id_driver_para).values()
        return all_order_in_delivery
    except:
        return False 

def update_status_order_by_code_order(code_order_para, status_update_para):
    try:
        update_status_order = Order_tb.objects.filter(code_order = code_order_para).update(status = status_update_para) 
        if update_status_order == 0:
            return False 
        else:
            return True
    except:
        return False