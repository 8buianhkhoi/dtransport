from django.urls import path

from . import views 

app_name = 'Driver_app'

urlpatterns = [
    path('', views.Base_page.as_view(), name = 'Driver_base_page')
]