from django.urls import path

from . import views 

app_name = 'Driver_app'

urlpatterns = [
    path('', views.Base_page.as_view(), name = 'Driver_base_page'),
    path('log-out/', views.log_out, name = 'Driver_log_out'),
    path('order-need-delivery/', views.Order_need_delivery.as_view(), name = 'Driver_order_need_delivery'),
    path('update-status-order/', views.Update_status_order.as_view(), name='Driver_update_status_order')
]