from django.shortcuts import render, redirect
from django.views import View 

from . import exec_data
# Create your views here.
def log_out(request):
    request.session.clear()
    return redirect('Homepage_app:Homepage_homepage')

class Base_page(View):
    def get(self, request):
        return render(request, 'Driver/base_page.html')

class Order_need_delivery(View):
    def get(self, request):
        id_driver = request.session.get('id_role_user_dt2023', None)
        
        all_order_need_delivery = exec_data.get_all_order_need_delivery(id_driver)
        context_return = {'all_order_need_delivery' : all_order_need_delivery}
        return render(request, 'Driver/order_need_delivery.html', context_return)

class Update_status_order(View):
    def get(self, request):
        return render(request, 'Driver/update_status_order.html')
    def post(self, request):
        get_code_order = request.POST.get('input_code_order', None)
        get_status_update = request.POST.get('select_status_order', None)

        context_return = {}
        update_status_order = exec_data.update_status_order_by_code_order(get_code_order, get_status_update)

        if update_status_order == True:
            context_return
        else:
            context_return['msg_update_status'] = False
        return render(request, 'Driver/update_status_order.html')


