from django.shortcuts import render, redirect
from django.views import View 

# Create your views here.
def log_out(request):
    request.session.clear()
    return redirect('Homepage_app:Homepage_homepage')

class Base_page(View):
    def get(self, request):
        return render(request, 'Driver/base_page.html')

