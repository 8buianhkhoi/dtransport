from django.shortcuts import render, redirect
from django.views import View 

# Create your views here.
class Base_page(View):
    def get(self, request):
        return render(request, 'Driver/base_page.html')
