from django.urls import path

from . import views

app_name = 'Company_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Company_homepage'),
    path('sales/', views.Sales.as_view(), name = 'Company_sales'),
    path('dispatcher/', views.Dispatcher.as_view(), name = 'Company_dispatcher'),
    path('get-all-province/', views.get_all_province, name = 'Company_get_all_province'),
    path('get-all-district/', views.get_district_by_province, name = 'Company_get_all_district'),
    path('get-all-ward/', views.get_ward_by_district_province, name = 'Company_get_all_ward'),
]