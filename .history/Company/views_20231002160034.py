from django.shortcuts import render
from django.views import View 

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Company/homepage.html')

class Sales(View):
    def get(self, request):
        return render(request, 'Company/sale.html')
    def post(self, request):
        new_user_name = request.POST.get('new_sale_username', None)
        new_password = request.POST.get('new_sale_password', None)
        new_gmail = request.POST.get('new_sale_gmail', None)
        new_tel = request.POST.get('new_sale_tel', None)
        new_gender = request.POST.get('new_sale_gender', None)
        new_province = request.POST.get('new_sale_province', None)
        new_district = request.POST.get('new_sale_district', None)
        new_ward = request.POST.get('new_sale_ward')

        return render(request, 'Company/sale.html')