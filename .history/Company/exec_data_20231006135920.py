from django.contrib.auth.hashers import make_password

from Homepage.models import *

def ins_new_sale(username_para, password_para, gmail_para, tel_para, gender_para, province_para, district_para, ward_para):
    try:
        # TODO : code here, sửa instance_id_company khi sửa xong phần đăng nhập
        instance_id_company = Company_tb.objects.get(pk = 1)

        new_sale = Sale_tb(username = username_para, password = make_password(password_para), gmail = gmail_para, tel = tel_para, id_company = instance_id_company, gender = gender_para, country = 'VN', province = province_para, district = district_para, ward = ward_para)
        new_sale.save()

        return True
    except:
        return False

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def ins_new_dispatcher(id_company_para, user_name_para, password_para, gmail_para, tel_para, gender_para, province_para, district_para, ward_para):
    try:
    except:
        return False