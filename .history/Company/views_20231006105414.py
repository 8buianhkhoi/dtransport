from django.shortcuts import render
from django.views import View 

from . import exec_data

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Company/homepage.html')

class Sales(View):
    def get(self, request):
        return render(request, 'Company/sale.html')
    def post(self, request):
        new_user_name = request.POST.get('new_sale_username', None)
        new_password = request.POST.get('new_sale_password', None)
        new_gmail = request.POST.get('new_sale_gmail', None)
        new_tel = request.POST.get('new_sale_tel', None)
        new_gender = request.POST.get('new_sale_gender', None)
        new_province = request.POST.get('new_sale_province', None)
        new_district = request.POST.get('new_sale_district', None)
        new_ward = request.POST.get('new_sale_ward')

        return_para = {'username_para' : new_user_name, 'password_para' : new_password, 'gmail_para' : new_gmail, 'tel_para' : new_tel, 'gender_para' : new_gender, 'province_para' : new_province, 'district_para' : new_district, 'ward_para' : new_ward}
        ins_new_sale = exec_data.ins_new_sale(**return_para)

        context_return = {}
        if ins_new_sale != False:
            context_return['ins_sale_mess'] = 'Thêm 1 sale mới thành công'
        else:
            context_return['ins_sale_mess'] = 'Thêm 1 sale thất bại'

        return render(request, 'Company/sale.html', context_return)

class Dispatcher(View):
    def get(self, request):
        return render(request, 'Company/dispatcher.html')