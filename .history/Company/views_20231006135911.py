from django.shortcuts import render
from django.views import View 
from django.http import JsonResponse

from . import exec_data

# Create your views here.
# 3 function help to load and get division
def get_all_province(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def get_district_by_province(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def get_ward_by_district_province(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

class Homepage(View):
    def get(self, request):
        return render(request, 'Company/homepage.html')

class Sales(View):
    def get(self, request):
        return render(request, 'Company/sale.html')
    def post(self, request):
        new_user_name = request.POST.get('new_sale_username', None)
        new_password = request.POST.get('new_sale_password', None)
        new_gmail = request.POST.get('new_sale_gmail', None)
        new_tel = request.POST.get('new_sale_tel', None)
        new_gender = request.POST.get('new_sale_gender', None)
        new_province = request.POST.get('new_sale_province', None)
        new_district = request.POST.get('new_sale_district', None)
        new_ward = request.POST.get('new_sale_ward')

        return_para = {'username_para' : new_user_name, 'password_para' : new_password, 'gmail_para' : new_gmail, 'tel_para' : new_tel, 'gender_para' : new_gender, 'province_para' : new_province, 'district_para' : new_district, 'ward_para' : new_ward}
        ins_new_sale = exec_data.ins_new_sale(**return_para)

        context_return = {}
        if ins_new_sale != False:
            context_return['ins_sale_mess'] = 'Thêm 1 sale mới thành công'
        else:
            context_return['ins_sale_mess'] = 'Thêm 1 sale thất bại'

        return render(request, 'Company/sale.html', context_return)

class Dispatcher(View):
    def get(self, request):
        return render(request, 'Company/dispatcher.html')
    def post(self, request):
        id_company = request.session.get('id_role_user_dt2023', None)

        new_username = request.POST.get('username_new_dispatcher', None)
        new_password = request.POST.get('password_new_dispatcher', None)
        new_gmail = request.POST.get('gmail_new_dispatcher', None)
        new_tel = request.POST.get('tel_new_dispatcher', None)
        new_gender = request.POST.get('gender_new_dispatcher', None)
        new_province = request.POST.get('province_new_dispatcher', None)
        new_district = request.POST.get('district_new_dispatcher', None)
        new_ward = request.POST.get('ward_new_dispatcher', None)

        return_para = {'id_company_para' : id_company, 'user_name_para' : new_username, 'password_para' : new_password, 'gmail_para' : new_gmail, 'tel_para' : new_tel, 'gender_para' : new_gender, 'province_para' : new_province, 'district_para' : new_district, 'ward_para' : new_ward}

        ins_new_dispatcher = exec_data.ins_new_dispatcher(**return_para)

        
        return render(request, 'Company/dispatcher.html')

