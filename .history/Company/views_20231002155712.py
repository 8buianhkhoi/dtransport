from django.shortcuts import render
from django.views import View 

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Company/homepage.html')

class Sales(View):
    def get(self, request):
        return render(request, 'Company/sale.html')
    def post(self, request):

        return render(request, 'Company/sale.html')