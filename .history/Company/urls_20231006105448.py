from django.urls import path

from . import views

app_name = 'Company_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Company_homepage'),
    path('sales/', views.Sales.as_view(), name = 'Company_sales'),
    path('dispatcher/', views.Dispatcher.as_view(), name = 'Company_dispatcher')
]