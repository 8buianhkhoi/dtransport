from django.contrib.auth.hashers import make_password

from .models import *

def check_login_pass_username(role_user_para ,username_para, pass_para):
    try:
        print(pass_para)
        if role_user_para == 'driver':
            get_user = Driver_tb.objects.filter(username = username_para, password = make_password(pass_para)).values()
        elif role_user_para == 'sale':
            get_user = Sale_tb.objects.filter(username = username_para, password = make_password(pass_para)).values()
        elif role_user_para == 'company':
            get_user = Company_tb.objects.filter(username = username_para, password = make_password(pass_para)).values()
        elif role_user_para == 'dispatcher':
            get_user = Dispatcher_tb.objects.filter(username = username_para, password = make_password(pass_para)).values()
        else:
            return False
        
        return get_user
    except:
        return False
