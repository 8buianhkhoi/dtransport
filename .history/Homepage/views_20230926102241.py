from django.shortcuts import render

# Create your views here.

def homepage(request):
    return render(request, 'Homepage/homepage.html')

def sign_up(request):
    return render(request, 'Homepage/sign_up.html')
