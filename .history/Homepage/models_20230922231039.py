from django.db import models

# Create your models here.

# Name spacing : name_name_name_tb
class Customer_tb(models.Model):
    class Choices(models.TextChoices):
        personal = 'personal', 'personal'
        company = 'company', 'company'
    gmail = models.CharField(max_length = 255)
    tel = models.CharField(max_length = 45)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    full_name = models.CharField(max_length = 255)
    gender = models.CharField(max_length = 255)
    types = models.CharField(max_length = 45, choices = Choices.choices)
    id_customer = models.CharField(max_length = 45)

class Shipping_addr_tb(models.Model):
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer', related_name = 'id')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district =  models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)
    types = models.CharField(max_length = 255,  choices = [('home', 'home'), ('company', 'company')])
    note = models.CharField(max_length = 1200)

class Delivery_addr_tb(models.Model):
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer', related_name = 'id')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district =  models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)
    types = models.CharField(max_length = 255,  choices = [('home', 'home'), ('company', 'company')])
    note = models.CharField(max_length = 1200)

class Company_tb(models.Model):
    full_name = models.CharField(max_length = 255)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    tin = models.CharField(max_length = 255)
    gmail = models.CharField(max_length = 255)
    tel = models.CharField(max_length = 45)
    account_bank = models.CharField(max_length = 255)
    name_bank = models.CharField(max_length = 255)
    name_host = models.CharField(max_length = 255)
    name_contact_person = models.CharField(max_length = 255)
    tel_contact_person = models.CharField(max_length = 255)
    gmail_contact_person = models.CharField(max_length = 255)
    url_website = models.CharField(max_length = 255)

class Driver_tb(models.Model):
    full_name = models.CharField(max_length = 255)
    gmail_driver = models.CharField(max_length = 255)
    tel_driver = models.CharField(max_length = 45)
    country_driver = models.CharField(max_length = 255)
    province_driver = models.CharField(max_length = 255)
    district_driver = models.CharField(max_length = 255)
    ward_driver = models.CharField(max_length = 255)
    id_driver = models.CharField(max_length = 11)
    gender_driver = models.CharField(max_length = 45, choices = [('men', 'men'), ('woman', 'woman'), ('other', 'other')])
    level_license_driver = models.CharField(max_length = 2, choices = [('A1', 'A1'), ('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E')])
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company', related_name = 'id')

class Car_tb(models.Model):
    license_plate_car = models.CharField(max_length = 255)
    type_car = models.CharField(max_length = 255, choices = [('motorbike', 'motorbike'), ('car_4', 'car_4'), ('car_7', 'car_7')])
    inspection_time = models.DateTimeField()
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver', related_name = 'id')
    id_car = models.CharField(max_length = 11)

class Inventory_tb(models.Model):
    name = models.CharField(max_length = 255)
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company', related_name = 'id')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    image = models.ImageField(upload_to = 'img_data/')

class Contract_tb(models.Model):
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company', related_name = 'id')
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer', related_name = 'id')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    date_sign = models.DateTimeField()
    cost = models.IntegerField()
    link_drive_scan = models.CharField(max_length = 1000)
    no_contract = models.CharField(max_length = 11)
    note = models.CharField(max_length = 1000)

class Order_tb(models.Model):
    id_contract = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'id_contract', related_name = 'id')
    code_order = models.CharField(max_length = 45)
    id_shipping_addr = models.ForeignKey(Shipping_addr_tb, on_delete = models.CASCADE, db_column = 'id_shipping_addr', related_name = 'id')
    id_delivery_addr = models.ForeignKey(Delivery_addr_tb, on_delete = models.CASCADE, db_column = 'id_delivery_addr', related_name = 'id')
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver', null = True, blank = True, related_name = 'id')
    date_create = models.DateTimeField(auto_now = True)
    departure_time = models.DateTimeField(null = True, blank = True)
    estimate_time_arrive = models.DateTimeField(null = True, blank = True)
    cost = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'cost', related_name = 'cost')
    note = models.CharField(max_length = 1000)
    status = models.CharField(max_length = 255, choices = [('Pending', 'Pending'), ('Need-Delivery', 'Need-Delivery'), ('Delivery', 'Delivery'), ('Success', 'Success'), ('Return-Order', 'Return-Order')])
    real_time_arrive = models.DateTimeField(null = True, blank = True)

class Sign_tb(models.Model):
    code = models.CharField(max_length = 11)
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver', related_name = 'id')
    status = models.CharField(max_length = 255, choices = [('Pick', 'Pick'), ('Pack', 'Pack'), ('Delivery', 'Delivery'), ('Finish', 'Finish'), ('Mistake', 'Mistake'), ('Fail', 'Fail')])
    note = models.CharField(max_length = 1000)

class Bill_tb(models.Model):
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver', related_name = 'id')
    id_car = models.ForeignKey(Car_tb, on_delete = models.CASCADE, db_column = 'id_car', related_name = 'id')
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer', related_name = 'id')
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company', related_name = 'id')
    start_time = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'start_time', related_name ='start_time')
    end_time = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'end_time', related_name = 'end_time')
    estimate_time = models.DateTimeField(blank = True, null = True)
    id_delivery_addr = models.ForeignKey(Delivery_addr_tb, on_delete = models.CASCADE, db_column = 'id_delivery_addr', related_name = 'id')
    id_sign = models.ForeignKey(Sign_tb, on_delete = models.CASCADE, db_column = 'id_sign', related_name = 'id')
    id_contract = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'id_contract', related_name = 'id')
    cost = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'cost', related_name = 'cost')

class Invoice_tb(models.Model):
    code = models.CharField(max_length = 11)
    id_contract = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'id_contract', related_name = 'id')
    dates = models.DateTimeField()
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company', related_name = 'id')
    license_plate_car = models.ForeignKey(Car_tb, on_delete = models.CASCADE, db_column = 'license_plate_car')
    full_name = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'full_name', related_name = 'full_name')
    id_shipping_addr = models.ForeignKey(Shipping_addr_tb, on_delete = models.CASCADE, db_column = 'id_shipping_addr', related_name = 'id')
    id_delivery_addr = models.ForeignKey(Delivery_addr_tb, on_delete = models.CASCADE, db_column = 'id_delivery_addr', related_name = 'id')
    pin_code = models.CharField(max_length = 255)
    ems_code = models.CharField(max_length = 255)
    start_date = models.DateTimeField()
    estimate_time = models.DateTimeField()
    real_time_arrive = models.DateTimeField()
    trip = models.CharField(max_length = 255, default = '')
    id_sign = models.ForeignKey(Sign_tb, on_delete = models.CASCADE, db_column = 'id_sign', related_name = 'id')

class Product_tb(models.Model):
    id_order = models.ForeignKey(Order_tb, on_delete = models.CASCADE, db_column = 'id_order', related_name = 'id')
    code = models.CharField(max_length = 255)
    product = models.JSONField()

class Division_tb(models.Model):
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)



