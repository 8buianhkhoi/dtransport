from django.urls import path

from . import views

app_name = 'Homepage_app'

urlpatterns = [
   path('', views.homepage, name = 'Homepage_homepage'),
   path('login/', views.login, name = 'Homepage_login')
]