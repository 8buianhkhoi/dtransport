from django.db import models

# Create your models here.

# Name spacing : name_name_name_tb
class Company_tb(models.Model):
    full_name = models.CharField(max_length = 255)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    tin = models.CharField(max_length = 255)
    gmail = models.CharField(max_length = 255)
    tel = models.CharField(max_length = 45)
    account_bank = models.CharField(max_length = 255)
    name_bank = models.CharField(max_length = 255)
    name_host = models.CharField(max_length = 255)
    name_contact_person = models.CharField(max_length = 255)
    tel_contact_person = models.CharField(max_length = 255)
    gmail_contact_person = models.CharField(max_length = 255)
    url_website = models.CharField(max_length = 255)

class Customer_tb(models.Model):
    gmail = models.CharField(max_length = 255)
    tel = models.CharField(max_length = 45)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    full_name = models.CharField(max_length = 255)
    gender = models.CharField(max_length = 255)
    types = models.CharField(max_length = 45, choices = [('personal', 'personal'), ('company', 'company')])

class Driver_tb(models.Model):
    gmail_driver = models.CharField(max_length = 255)
    tel_driver = models.CharField(max_length = 45)
    country_driver = models.CharField(max_length = 255)
    province_driver = models.CharField(max_length = 255)
    district_driver = models.CharField(max_length = 255)
    ward_driver = models.CharField(max_length = 255)
    id_driver = models.CharField(max_length = 11)
    gender_driver = models.CharField(max_length = 45, choices = [('men', 'men'), ('woman', 'woman'), ('other', 'other')])
    level_license_driver = models.CharField(max_length = 2, choices = [('A1', 'A1'), ('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E')])
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company')

class Car_tb(models.Model):
    license_plate_car = models.CharField(max_length = 255)
    type_car = models.CharField(max_length = 255, choices = [('motorbike', 'motorbike'), ('car_4', 'car_4'), ('car_7', 'car_7')])
    inspection_time = models.DateTimeField()
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver')
    id_car = models.CharField(max_length = 11)

class Inventory_tb(models.Model):
    name = models.CharField(max_length = 255)
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    image = models.ImageField(upload_to = 'img_data/')

class Contract_tb(models.Model):
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company')
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer')
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    date_sign = models.DateTimeField()
    cost = models.IntegerField()
    link_drive_scan = models.CharField(max_length = 1000)
    no_contract = models.CharField(max_length = 11)
    note = models.CharField(max_length = 1000)

class Shipping_addr_tb(models.Model):
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district =  models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)
    types = models.CharField(max_length = 255,  choices = [('home', 'home'), ('company', 'company')])
    note = models.CharField(max_length = 1200)

class Delivery_addr_tb(models.Model):
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer')
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district =  models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)
    types = models.CharField(max_length = 255,  choices = [('home', 'home'), ('company', 'company')])
    note = models.CharField(max_length = 1200)

class Order_tb(models.Model):
    code_order = models.CharField(max_length = 45)
    pass

class Bill_tb(models.Model):
    id_driver = models.ForeignKey(Driver_tb, on_delete = models.CASCADE, db_column = 'id_driver')
    id_car = models.ForeignKey(Car_tb, on_delete = models.CASCADE, db_column = 'id_car')
    id_customer = models.ForeignKey(Customer_tb, on_delete = models.CASCADE, db_column = 'id_customer')
    id_company = models.ForeignKey(Company_tb, on_delete = models.CASCADE, db_column = 'id_company')
    start_time = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'start_time')
    end_time = models.ForeignKey(Contract_tb, on_delete = models.CASCADE, db_column = 'end_time')
    estimate_time = models.DateTimeField()
    pass





