from django.shortcuts import render
from django.views import View
import datetime
import hashlib
import os
import base64
from dotenv import load_dotenv
import jwt

# Create your views here.

def homepage(request):
    return render(request, 'Homepage/homepage.html') 

class login(View):
    def get(self, request):
        return render(request, 'Homepage/login.html')
