from django.shortcuts import render, redirect
from django.views import View
import datetime
import hashlib
import os
import base64
from dotenv import load_dotenv
import jwt


from . import exec_data

# Create your views here. 
class Homepage_page(View):
    def get(self, request):
        return render(request, 'Homepage/homepage.html')
    def post(self, request):
        get_code_order = request.POST.get('pre_header_search', None)
        return redirect('Homepage_app:Homepage_search_order', code_order = get_code_order)

class login(View):
    def get(self, request):
        return render(request, 'Homepage/login.html')
    def post(self, request):
        load_dotenv()
        secret_key = os.getenv('secret_key')
        salt_key = os.getenv('salt_key')

        request.session.clear()

        role_select = request.POST.get('login_choose_role', None)
        get_username = request.POST.get('user_name_login', None)
        get_password = request.POST.get('pass_login', None)

        exec_check_login = exec_data.check_login_pass_username(role_select, get_username, get_password, salt_key)

        if (exec_check_login != False) and (len(exec_check_login) == 1):
            start_time_token = datetime.datetime.now().strftime(f'%d-%m-%Y %H:%M:%S')
            end_time_token = (datetime.datetime.now() + datetime.timedelta(days = 2)).strftime(f'%d-%m-%Y %H:%M:%S')

            dict_token = {'role_user' : role_select, 'id_role_user' : exec_check_login[0]['id'], 'start_time' : start_time_token, 'end_time' : end_time_token}

            request.session['token_dt2023_web_demo'] = jwt.encode(dict_token, secret_key, algorithm='HS256')
            request.session['role_user_dt2023'] = role_select
            request.session['id_role_user_dt2023'] = exec_check_login[0]['id']

            if role_select == 'driver':
                return redirect('Driver_app:Driver_base_page')
            elif role_select == 'company':
                return redirect('Company_app:Company_homepage') 
            elif role_select == 'sale':
                return redirect('Sales_app:Sales_homepage')
            elif role_select == 'dispatcher':
                return redirect('Dispatcher_app:Dispatcher_homepage')
            elif role_select == 'host':
                return redirect('Host_app:Host_base_page')
        else:
            return render(request, 'Homepage/login.html', {'notification_login' : 'Đăng nhập thất bại. Xem lại mật khẩu và tên đăng nhập'})

        return render(request, 'Homepage/login.html')

class Search_order(View):
    def get(self, request, code_order):
        result_search_order = exec_data.get_order_by_code_order(code_order)
        if result_search_order != False and len(result_search_order) == 0:
            result_search_order = []

        context_return = {}
        context_return['code_order'] = code_order
        context_return['result_search_order'] = result_search_order

        return render(request, 'Homepage/search_order.html', context_return)