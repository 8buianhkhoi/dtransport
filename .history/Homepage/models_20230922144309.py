from django.db import models

# Create your models here.

# Name spacing : name_name_name_tb
class Company_tb(models.Model):
    full_name_company = models.CharField(max_length = 255)
    country_company = models.CharField(max_length = 255)
    province_company = models.CharField(max_length = 255)
    district_company = models.CharField(max_length = 255)
    ward_company = models.CharField(max_length = 255)
    tin_company = models.CharField(max_length = 255)
    gmail_company = models.CharField(max_length = 255)
    tel_company = models.CharField(max_length = 45)
    account_bank_company = models.CharField(max_length = 255)
    name_bank_company = models.CharField(max_length = 255)
    name_host_company = models.CharField(max_length = 255)
    name_contact_person_company = models.CharField(max_length = 255)
    tel_contact_person_company = models.CharField(max_length = 255)
    gmail_contact_person_company = models.CharField(max_length = 255)
    url_website_company = models.CharField(max_length = 255)

class Customer_tb(models.Model):
    gmail_customer = models.CharField(max_length = 255)
    tel_customer = models.CharField(max_length = 45)
    country_customer = models.CharField(max_length = 255)
    province_customer = models.CharField(max_length = 255)
    district_customer = models.CharField(max_length = 255)
    ward_customer = models.CharField(max_length = 255)
    full_name_customer = models.CharField(max_length = 255)
    gender_customer = models.CharField(max_length = 255)

class Driver_tb(models.Model):
    gmail_driver = models.CharField(max_length = 255)
    tel_driver = models.CharField(max_length = 45)
    country_driver = models.CharField(max_length = 255)
    province_driver = models.CharField(max_length = 255)
    district_driver = models.CharField(max_length = 255)
    ward_driver = models.CharField(max_length = 255)
    id_driver = models.CharField(max_length = 11)
    gender_driver = models.CharField(max_length = 45, choices = [('men', 'men'), ('woman', 'woman'), ('other', 'other')])
    level_license_driver = models.CharField(max_length = 2, choices = [('A1', 'A1'), ('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'), ('E', 'E')])
    id_company = models.Foreign(Company_tb, on_delete = models.CASCADE, db_column = 'id_company')



