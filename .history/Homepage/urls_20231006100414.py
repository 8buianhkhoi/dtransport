from django.urls import path

from . import views

app_name = 'Homepage_app'

urlpatterns = [
   path('', views.Homepage_page.as_view(), name = 'Homepage_homepage'),
   path('login/', views.login.as_view(), name = 'Homepage_login'),
   path('search-order/', views.Search_order.as_view(), name ='Homepage_search_order')
]