from django.shortcuts import render
from django.views import View
import datetime
import hashlib
import os
import base64
from dotenv import load_dotenv
import jwt


from . import exec_data

# Create your views here.

def homepage(request):
    return render(request, 'Homepage/homepage.html') 

class login(View):
    def get(self, request):
        return render(request, 'Homepage/login.html')
    def post(self, request):
        load_dotenv()
        secret_key = os.getenv('secret_key')
        salt_key = os.getenv('salt_key')

        request.session.clear()

        start_time_token = datetime.datetime.now().strftime(f'%d-%m-%Y %H:%M:%S')
        end_time_token = (datetime.datetime.now() + datetime.timedelta(days = 2)).strftime(f'%d-%m-%Y %H:%M:%S')

        role_select = request.POST.get('login_choose_role', None)
        get_username = request.POST.get('user_name_login', None)
        get_password = request.POST.get('pass_login', None)

        exec_check_login = exec_data.check_login_pass_username(role_select, get_username, get_password, salt_key)

        print('----------------')
        print(exec_check_login[0])
        # TODO
        # dict_token = {'roleUser' : role_user, 'idRoleUser' : , 'startTimeTokenStr' : startTimeTokenStr, 'endTimeTokenStr' : endTimeTokenStr}

        # request.session['token_ddelivery_web_demo'] = jwt.encode(dictTokenStr, secret_key, algorithm='HS256')
        return render(request, 'Homepage/login.html')