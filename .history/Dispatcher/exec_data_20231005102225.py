import datetime
import uuid

from Homepage.models import *

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def get_all_customer_by_id_company(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_customer = Customer_tb.objects.filter(id_company = id_company, status = 'signoff').values()

        return all_customer
    except:
        return False

def get_all_contract_by_id_customer(id_customer_para):
    try:
        all_contract = Contract_tb.objects.filter(id_customer = id_customer_para, status = 'on').values()

        return all_contract
    except:
        return False

def get_all_driver_by_id_dispatcher(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_driver = Driver_tb.objects.filter(id_company = id_company).values()

        return all_driver
    except:
        return False

def ins_new_order(no_contract_para, province_sender_para, district_sender_para, ward_sender_para, province_receiver_para, district_receiver_para, ward_receiver_para, id_driver_para, departure_time_para, estimate_time_arrive_para, note_para, real_time_arrive_para, type_sender_para, type_receiver_para):
    try:
        get_contract = Contract_tb.objects.filter(no_contract = no_contract_para).values('id', 'cost', 'id_customer')[0]
        instance_id_customer = Customer_tb.objects.get(pk = get_contract['id_customer'])
        
        ins_shipping_addr = Shipping_addr_tb(id_customer = instance_id_customer, country = 'VN', province = province_sender_para, district = district_sender_para, ward = ward_sender_para, latitude = '', longitude = '', types = type_sender_para, note = '')
        ins_shipping_addr.save()

        ins_delivery_addr = Delivery_addr_tb(id_customer = instance_id_customer, country = 'VN', province = province_receiver_para, district = district_receiver_para, ward = ward_receiver_para, latitude = '', longitude = '', types = type_receiver_para, note = '')
        ins_delivery_addr.save()

        instance_id_shipping_addr = Shipping_addr_tb.objects.get(pk = ins_shipping_addr.id)
        instance_id_delivery_addr = Delivery_addr_tb.objects.get(pk = ins_delivery_addr.id)
        instance_id_contract = Contract_tb.objects.get(pk = get_contract['id'])
        instance_id_driver = Driver_tb.objects.get(pk = id_driver_para)
        instance_cost_contract = Contract_tb.objects.get(cost = get_contract['cost'])

        code_order_random = str(no_contract_para) + "__" + datetime.datetime.now().strftime(f'%Y-%m-%d-%H:%M:%S') + "__" + str(uuid.uuid4())
        ins_order = Order_tb(id_contract = instance_id_contract, code_order = code_order_random, id_shipping_addr = instance_id_shipping_addr, id_delivery_addr = instance_id_delivery_addr, id_driver = instance_id_driver, departure_time = departure_time_para, estimate_time_arrive = estimate_time_arrive_para, cost = instance_cost_contract, note = note_para, status = 'Delivery', real_time_arrive = real_time_arrive_para)
        ins_order.save()

        Contract_tb.objects.filter(no_contract = no_contract_para).update(status = 'delivery')
        return code_order_random
    except:
        return False

def show_all_order_by_id_company(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_contract = Contract_tb.objects.filter(id_company = id_company).values('id')
        lst_id_contract = []
        lst_order = []

        for index in all_contract:
            lst_id_contract.append(index['id'])
        for index in lst_id_contract:
            print(index)
        
    except:
        return False