from django.shortcuts import render, redirect
from django.views import View 
from django.http import JsonResponse

from . import  exec_data

# Create your views here.

# 3 function help to load and get division
def get_all_province(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def get_district_by_province(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def get_ward_by_district_province(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)
    
class Homepage(View):
    def get(self, request):
        return render(request, 'Dispatcher/base_page.html')
    
class Get_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        context_return = {}

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        return render(request, 'Dispatcher/get_order.html', context_return)
    def post(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        new_id_customer = request.POST.get('select_customer', None)
        get_all_contract = exec_data.get_all_contract_by_id_customer(new_id_customer)

        context_return = {}

        if get_all_contract != False:
            context_return['all_contract'] = get_all_contract
        else:
            context_return['all_contract'] = False

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        return render(request, 'Dispatcher/get_order.html', context_return)

class Send_order(View):
    def get(self, request, no_contract):
        id_dispatcher = request.session.get('id_role_user_dt2023', None)
        return render(request, 'Dispatcher/send_order.html')