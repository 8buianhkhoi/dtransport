from django.shortcuts import render, redirect
from django.views import View 
from django.http import JsonResponse

from . import  exec_data

# Create your views here.

# 3 function help to load and get division
def get_all_province(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def get_district_by_province(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def get_ward_by_district_province(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

def log_out(request):
    request.session.clear()
    return redirect('Homepage_app:Homepage_homepage')

class Homepage(View):
    def get(self, request):
        return render(request, 'Dispatcher/base_page.html')
    
class Get_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        context_return = {}
        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        context_return['all_contract'] = False
        return render(request, 'Dispatcher/get_order.html', context_return)
    def post(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        new_id_customer = None
        for index in get_all_customer:
            new_id_customer_temp = request.POST.get(f'input_submit_{index["id"]}', None)
            if new_id_customer_temp is not None:
                new_id_customer = index["id"]
            
        get_all_contract = exec_data.get_all_contract_by_id_customer(new_id_customer)

        context_return = {}

        if get_all_contract != False:
            context_return['all_contract'] = get_all_contract
        else:
            context_return['all_contract'] = False

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        return render(request, 'Dispatcher/get_order.html', context_return)

class Send_order(View):
    def get(self, request, no_contract):
        id_dispatcher = request.session.get('id_role_user_dt2023', None)
        get_all_driver = exec_data.get_all_driver_by_id_dispatcher(id_dispatcher)
        context_return = {}

        if get_all_driver != False:
            context_return['all_driver'] = get_all_driver
        else:
            context_return['msg_load'] = False

        return render(request, 'Dispatcher/send_order.html', context_return)
    def post(self, request, no_contract):
        # id_dispatcher = request.session.get('id_role_user_dt2023', None)
        # get_all_driver = exec_data.get_all_driver_by_id_dispatcher(id_dispatcher)
        
        # new_province_sender = request.POST.get('get_province_sender', None)
        # new_district_sender = request.POST.get('get_district_sender', None)
        # new_ward_sender = request.POST.get('get_ward_sender', None)
        # new_type_sender = request.POST.get('get_type_sender', None)

        # new_province_receiver = request.POST.get('get_province_receiver', None)
        # new_district_receiver = request.POST.get('get_district_receiver', None)
        # new_ward_receiver = request.POST.get('get_ward_receiver', None)
        # new_type_receiver = request.POST.get('get_type_receiver', None)

        # new_id_driver = request.POST.get('get_driver_select', None)
        # new_departure_time = request.POST.get('get_departure_time', None)
        # new_estimate_time = request.POST.get('get_estimate_time', None)
        # new_note = request.POST.get('get_note', None)
        # new_real_time_arrive = request.POST.get('get_real_time', None)

        # return_order = {'no_contract_para' : no_contract, 'province_sender_para' : new_province_sender, 'district_sender_para' : new_district_sender, 'ward_sender_para' : new_ward_sender, 'province_receiver_para' : new_province_receiver, 'district_receiver_para' : new_district_receiver, 'ward_receiver_para' : new_ward_receiver, 'id_driver_para' : new_id_driver, 'departure_time_para' : new_departure_time, 'estimate_time_arrive_para' : new_estimate_time, 'note_para' : new_note, 'real_time_arrive_para' : new_real_time_arrive, 'type_sender_para' : new_type_sender, 'type_receiver_para' : new_type_receiver}

        context_return = {}

        # ins_new_order = exec_data.ins_new_order(**return_order)
        
        # if ins_new_order != False:
        #     context_return['new_order_status'] = f'Tạo đơn hàng thành công. Mã đơn hàng là {ins_new_order}'
        # else:
        #     context_return['new_order_status'] = False

        # if get_all_driver != False:
        #     context_return['all_driver'] = get_all_driver
        # else:
        #     context_return['msg_load'] = False

        print('---------------------------------------------')
        print(request.session.get('input_list_product', None))
        return render(request, 'Dispatcher/send_order.html', context_return)

class Show_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023', None)

        all_order = exec_data.show_all_order_by_id_company(id_dispatcher)
        context_return = {}

        if all_order != False:
            context_return['all_order'] = all_order
        else:
            context_return['all_order'] = False

        return render(request, 'Dispatcher/show_order.html', context_return)
