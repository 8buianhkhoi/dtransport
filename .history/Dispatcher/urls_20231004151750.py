from django.urls import path

from . import views 

app_name = 'Dispatcher_app'

urlpatterns = [
    path('get-all-province/', views.get_all_province, name = 'Dispatcher_get_all_province'),
    path('get-all-district/', views.get_district_by_province, name = 'Dispatcher_get_all_district'),
    path('get-all-ward/', views.get_ward_by_district_province, name = 'Dispatcher_get_all_ward'),
    path('', views.Homepage.as_view(), name = 'Dispatcher_homepage'),
    path('get-order/', views.Get_order.as_view(), name = 'Dispatcher_get_order'),
    path('get-order/send-order/<str:no_contract>/', views.Send_order.as_view(), name = 'Dispatcher_send_order')    
]