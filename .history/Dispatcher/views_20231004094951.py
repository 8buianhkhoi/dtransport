from django.shortcuts import render, redirect
from django.views import View 

from . import  exec_data

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Dispatcher/base_page.html')
    
class Get_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        exec_data.get_all_customer_by_id_company(id_dispatcher)
        return render(request, 'Dispatcher/get_order.html')
