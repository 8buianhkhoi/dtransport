function load_district(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    let province = document.getElementById('choose__province__sender').value
    $.ajax({
        url:'/dispatcher/get-all-district/',
        method: 'POST',
        data: {province:province, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementById('choose__district__sender')
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function load_ward(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 

    let district = document.getElementById('choose__district__sender').value
    let province = document.getElementById('choose__province__sender').value
    $.ajax({
        url:'/dispatcher/get-all-ward/',
        method: 'POST',
        data: {province:province, district :district, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementById('choose__ward__sender')
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function on_load_province(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    $.ajax({
        url:'/dispatcher/get-all-province/',
        method: 'POST',
        data: {
            csrfmiddlewaretoken : csrf_token,
        },
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('select__province__sender');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

// -------------------------------------------------------------------------------------------------------------
function load_district_receiver(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    let province = document.getElementById('choose__province__receiver').value
    $.ajax({
        url:'/dispatcher/get-all-district/',
        method: 'POST',
        data: {province:province, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementById('choose__district__receiver')
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function load_ward_receiver(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 

    let district = document.getElementById('choose__district__receiver').value
    let province = document.getElementById('choose__province__receiver').value
    $.ajax({
        url:'/dispatcher/get-all-ward/',
        method: 'POST',
        data: {province:province, district :district, csrfmiddlewaretoken : csrf_token},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementById('choose__ward__receiver')
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function on_load_province_receiver(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    $.ajax({
        url:'/dispatcher/get-all-province/',
        method: 'POST',
        data: {
            csrfmiddlewaretoken : csrf_token,
        },
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('select__province__receiver');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}


function add_product_to_list_product(){
    let current_list_product = document.getElementById('list__product__value').value
    let json_current_list_product = JSON.parse(current_list_product)

    let get_table = document.getElementById('show__list__product__id');
    let new_row = get_table.insertRow()
    let new_col_1  = new_row.insertCell(0);
    let new_col_2  = new_row.insertCell(1);
    let new_col_3  = new_row.insertCell(2);

    new_col_1.innerText = String(get_table.rows.length - 1)
    new_col_2.innerText = document.getElementById('name__product__input__id').value
    new_col_3.innerText = document.getElementById('quantity__product__input__id').value

    json_current_list_product[String(get_table.rows.length - 1)] = {String(document.getElementById('name__product__input__id').value) : String(document.getElementById('quantity__product__input__id').value)}
}