from django.shortcuts import render, redirect
from django.views import View 

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Dispatcher/base_page.html')
    
class Get_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        return render(request, 'Dispatcher/get_order.html')
