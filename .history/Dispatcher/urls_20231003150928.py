from django.urls import path

from . import views 

app_name = 'Dispatcher_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Dispatcher_homepage')    
]