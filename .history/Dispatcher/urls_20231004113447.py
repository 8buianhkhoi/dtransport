from django.urls import path

from . import views 

app_name = 'Dispatcher_app'

urlpatterns = [
    path('', views.Homepage.as_view(), name = 'Dispatcher_homepage'),
    path('get-order/', views.Get_order.as_view(), name = 'Dispatcher_get_order'),
    path('get-order/send-order/<str:no_contract>/', views.Send_order.as_view(), name = 'Dispatcher_send_order')    
]