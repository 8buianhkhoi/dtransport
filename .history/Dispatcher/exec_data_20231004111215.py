from Homepage.models import *

def get_all_customer_by_id_company(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_customer = Customer_tb.objects.filter(id_company = id_company, status = 'signoff').values()

        return all_customer
    except:
        return False

def get_all_contract_by_id_customer(id_customer_para):
    try:
        all_contract = Contract_tb.objects.filter(id_customer = id_customer_para, status = 'on').values()

        return all_contract
    except:
        return False