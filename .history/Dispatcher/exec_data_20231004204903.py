from Homepage.models import *

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def get_all_customer_by_id_company(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_customer = Customer_tb.objects.filter(id_company = id_company, status = 'signoff').values()

        return all_customer
    except:
        return False

def get_all_contract_by_id_customer(id_customer_para):
    try:
        all_contract = Contract_tb.objects.filter(id_customer = id_customer_para, status = 'on').values()

        return all_contract
    except:
        return False

def get_all_driver_by_id_dispatcher(id_dispatcher_para):
    try:
        id_company = Dispatcher_tb.objects.filter(id = id_dispatcher_para).values('id_company')[0]['id_company']
        all_driver = Driver_tb.objects.filter(id_company = id_company).values()

        return all_driver
    except:
        return False

def ins_new_order(no_contract_para, province_sender_para, district_sender_para, ward_sender_para, province_receiver_para, district_receiver_para, ward_receiver_para, id_driver_para, departure_time_para, estimate_time_arrive_para, note_para, real_time_arrive_para):
    try:
        id_and_cost_contract = Contract_tb.objects.filter(no_contract = no_contract_para).values('id', 'cost')[0]
        print('---------------')
        print(id_and_cost_contract)
    except:
        return False