from django.shortcuts import render, redirect
from django.views import View 

from . import  exec_data

# Create your views here.
class Homepage(View):
    def get(self, request):
        return render(request, 'Dispatcher/base_page.html')
    
class Get_order(View):
    def get(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        context_return = {}

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        return render(request, 'Dispatcher/get_order.html', context_return)
    def post(self, request):
        id_dispatcher = request.session.get('id_role_user_dt2023')
        get_all_customer = exec_data.get_all_customer_by_id_company(id_dispatcher)

        new_id_customer = request.POST.get('select_customer', None)
        get_all_contract = exec_data.get_all_contract_by_id_customer(new_id_customer)

        context_return = {}

        if get_all_contract != False:
            context_return['all_contract'] = get_all_contract
        else:
            context_return['all_contract'] = False

        if get_all_customer != False:
            context_return['all_customer'] = get_all_customer
        else:
            context_return['all_customer'] = False

        return render(request, 'Dispatcher/get_order.html', context_return)