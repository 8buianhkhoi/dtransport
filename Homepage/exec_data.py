from django.contrib.auth.hashers import make_password

from .models import *

def check_login_pass_username(role_user_para ,username_para, pass_para, salt_key):
    try:
        if role_user_para == 'driver':
            get_user = Driver_tb.objects.filter(username = username_para, password = make_password(pass_para, salt = salt_key)).values()
        elif role_user_para == 'sale':
            get_user = Sale_tb.objects.filter(username = username_para, password = make_password(pass_para, salt = salt_key)).values()
        elif role_user_para == 'company':
            get_user = Company_tb.objects.filter(username = username_para, password = make_password(pass_para, salt = salt_key)).values()
        elif role_user_para == 'dispatcher':
            get_user = Dispatcher_tb.objects.filter(username = username_para, password = make_password(pass_para, salt = salt_key)).values()
        elif role_user_para == 'host':
            get_user = Host_tb.objects.filter(username = username_para, password = make_password(pass_para, salt = salt_key)).values()
        else:
            return False
        
        return get_user
    except Exception as e:
        print(e)
        return False

def get_order_by_code_order(code_order_para):
    try:
        get_order = Order_tb.objects.filter(code_order = code_order_para).values()
        return get_order
    except:
        return False
