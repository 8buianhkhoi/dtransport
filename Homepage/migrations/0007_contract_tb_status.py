# Generated by Django 4.1.7 on 2023-10-04 04:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Homepage', '0006_customer_tb_id_company'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract_tb',
            name='status',
            field=models.CharField(choices=[('on', 'on'), ('off', 'off'), ('cancel', 'cancel')], default='on', max_length=45),
            preserve_default=False,
        ),
    ]
