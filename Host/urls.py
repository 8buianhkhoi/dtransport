from django.urls import path

from . import views 

app_name = 'Host_app'

urlpatterns = [
    path('', views.base_page, name = 'Host_base_page'),
    path('company/', views.Company.as_view(), name = 'Host_company'),
    path('get-all-province/', views.get_all_province, name = 'Host_get_all_province'),
    path('get-all-district/', views.get_district_by_province, name = 'Host_get_all_district'),
    path('get-all-ward/', views.get_ward_by_district_province, name = 'Host_get_all_ward'),
    path('log-out/', views.log_out,  name='Host_log_out')
]