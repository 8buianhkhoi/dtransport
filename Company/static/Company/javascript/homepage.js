var dropdown_btn = document.getElementsByClassName('dropdown__btn__employee')
var i;

for (i = 0; i < dropdown_btn.length; i++){
    
    dropdown_btn[i].addEventListener('click', function(){
        this.classList.toggle('active__link')
        var dropdown_content = this.nextElementSibling;
        if(dropdown_content.style.display === 'block'){
            dropdown_content.style.display = 'none'
        } else {
            dropdown_content.style.display = 'block'
        }

    })
}